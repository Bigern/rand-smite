<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>
#bannerTop {
	max-width:100%;
	max-height:100%;
}

#imageContainer {
	background: #000;
	max-height:150px;
}
</style>
<div class="row" id="imageContainer">
	<div class="col-lg-2">
		<input type="text" class="form-control" style="visibility:hidden">
	</div>
	<div class="col-lg-8 col-md-8 col-sm-6" id="imageContainer">
		<img id="bannerTop" src="<c:url value="/resources/images/Logos/Logo_Smite_2013_.png"/>" alt="Banner">
	</div>
	<div class="col-lg-2">
		<input type="text" class="form-control" style="visibility:hidden">
	</div>
</div>
<!-- Start of Primary Navigation -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<c:url value="/randomgod/init"/>">Random Smite</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="<c:url value="/randomgod/init"/>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/randomgod/init"/>">Random God</a></li>
            <li class="divider"></li>
            <li><a href="<c:url value="/randomgod/init"/>">Random God</a></li>
            <li class="divider"></li>
            <li><a href="<c:url value="/randomgod/init"/>">Random God</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<c:url value="/randomgod/init"/>">Link</a></li>
        <li class="dropdown">
          <a href="<c:url value="/randomgod/init"/>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/randomgod/init"/>">Action</a></li>
            <li><a href="<c:url value="/randomgod/init"/>">Another action</a></li>
            <li><a href="<c:url value="/randomgod/init"/>">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="<c:url value="/randomgod/init"/>">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>