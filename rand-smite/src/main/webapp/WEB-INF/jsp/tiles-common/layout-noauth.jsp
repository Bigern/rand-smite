<!DOCTYPE html>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:importAttribute name="title" toName="pageTitle" scope="page"/>
<html>

    <head>

		<meta charset="UTF-8">
		<title><spring:message code="${pageTitle}"/></title>
		
		<tiles:importAttribute name="javascript" toName="jsImports"/>
		<c:forEach var="jsItem" items="${jsImports}">
            <tiles:insertAttribute value="${jsItem}"/>
		</c:forEach>
		<tiles:importAttribute name="css" toName="cssImports"/>
        <c:forEach var="cssItem" items="${cssImports}">
            <tiles:insertAttribute value="${cssItem}"/>
        </c:forEach>
		
	</head>

    <body>

	<div class="container">
		<div id="pagecontainer">
			<div id="containerwrapper">

				<div class="header-container">
					<tiles:insertAttribute name="header" />
					<tiles:insertAttribute name="menu" />
				</div>

				<div class="breadcrumb-container">
					<tiles:insertAttribute name="breadcrumb" />
				</div>

				<div class="body-container">
					<tiles:insertAttribute name="body" />
					<tiles:insertAttribute name="footer" />
				</div>
				
			</div>
		</div>
	</div>

    </body>

</html>