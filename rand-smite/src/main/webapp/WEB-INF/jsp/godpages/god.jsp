<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
#godPortrait {
	max-height: auto;
	max-width: 100%;
}
#ability-tab-content {
	color: #000;
}

#abilityTabs img {
	max-height: 64px;
	max-width: 64px;
}
</style>
<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">${god.name} - ${god.title}</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<img id="godPortrait" src="<c:url value="/resources/images/character-cards/card_${god.id}.jpg"/>" alt="Banner">
					</div>
					<div class="col-md-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Abilities</h3>
							</div>
							<div class="panel-body">
								<ul id="abilityTabs" data-tabs="abilityTabs" class="nav nav-tabs">
									<li class="active"><a href="#passiveTab" data-toggle="tab"><img id="passiveAbility" src="<c:url value="/resources/images/abilities/${god.name.concat(' - ').concat(god.ability5)}.jpg"/>" alt="Banner"></a></li>
									<li><a href="#ability1Tab" data-toggle="tab"><img id="ability1" src="<c:url value="/resources/images/abilities/${god.name} - ${god.ability1}.jpg"/>" alt="Banner"></a></li>
									<li><a href="#ability2Tab" data-toggle="tab"><img id="ability2" src="<c:url value="/resources/images/abilities/${god.name} - ${god.ability2}.jpg"/>" alt="Banner"></a></li>
									<li><a href="#ability3Tab" data-toggle="tab"><img id="ability3" src="<c:url value="/resources/images/abilities/${god.name} - ${god.ability3}.jpg"/>" alt="Banner"></a></li>
									<li><a href="#ability4Tab" data-toggle="tab"><img id="ability4" src="<c:url value="/resources/images/abilities/${god.name} - ${god.ability4}.jpg"/>" alt="Banner"></a></li>
								</ul>
								<div id="ability-tab-content" class="tab-content">
									<div class="tab-pane active" id="passiveTab">
							            <h3>${god.ability5}</h3>
							            <hr>
							            <p>${god.abilityDescription5.itemDescription.description}</p>
							        </div>
							        <div class="tab-pane" id="ability1Tab">
							            <h3>${god.ability1}</h3>
							            <hr>
							            <p>${god.abilityDescription1.itemDescription.description}</p>
							        </div>
							        <div class="tab-pane" id="ability2Tab">
							            <h3>${god.ability2}</h3>
							            <hr>
							            <p>${god.abilityDescription2.itemDescription.description}</p>
							        </div>
							        <div class="tab-pane" id="ability3Tab">
							            <h3>${god.ability3}</h3>
							            <hr>
							            <p>${god.abilityDescription4.itemDescription.description}</p>
							        </div>
							        <div class="tab-pane" id="ability4Tab">
							            <h3>${god.ability4}</h3>
							            <hr>
							            <p>${god.abilityDescription4.itemDescription.description}</p>
							        </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#abilityTabs').tab();
});

</script>
