<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url value="/randomgod/init" var="godUrl" />

<script type="javascript">
$(document).ready(function() {
	
});
</script>

<div class="container">
	<div class="jumbotron">
		<h2>Random God</h2>
		<a id="randomButton" class="linkAsButton" href="${godUrl}" title="Random God">Random God</a>
	</div>
</div>