package com.randsmite.web.controller.welcome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.randsmite.web.utils.APICallerService;
import com.randsmite.web.utils.GodManager;

@Controller
public class WelcomeController {

	private final String WELCOME_VIEW_NAME = "welcome";

	@Autowired
	private APICallerService apiSetupHelper;

	@Autowired
	private GodManager godManager;

	@RequestMapping("/")
	public String init() {
		return WELCOME_VIEW_NAME;
	}

	@RequestMapping("/welcome")
	public String init2() {
		return WELCOME_VIEW_NAME;
	}
}
