package com.randsmite.web.controller.random;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.randsmite.web.entity.God;
import com.randsmite.web.utils.GodManager;

@Controller
@RequestMapping("/randomgod")
public class RandomController {

	private final String RANDOM_VIEW_NAME = "randomGod";

	@Autowired
	private GodManager godManager;

	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public ModelAndView init(ModelMap model) {
		final ModelAndView mav = new ModelAndView(RANDOM_VIEW_NAME);
		final List<God> godList = godManager.getGodList();
		if (godList != null) {
			System.out.println("God List Size: " + godList.size());
		}
		final Random rand = new Random();
		final int randomNum = rand.nextInt(godList.size() - 1 - 0 + 1) + 0;
		final God randomGod = godList.get(randomNum);
		System.out.println("God Generated: " + randomGod.getName());
		model.addAttribute("god", randomGod);
		return mav;

	}
}
