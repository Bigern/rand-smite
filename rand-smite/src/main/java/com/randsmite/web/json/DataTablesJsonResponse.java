package com.randsmite.web.json;


/**
 * The Class DataTablesJsonResponse.
 */
public class DataTablesJsonResponse extends DataJsonResponse {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3736119261235178768L;
    
    private int sEcho;
    
    private int iTotalRecords;
    
    private int iTotalDisplayRecords;
    
    private int iDisplayStart;
    
    private Boolean returnToStart;
	
	public int getsEcho() {
		return sEcho;
	}

	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Boolean getReturnToStart() {
		return returnToStart;
	}

	public void setReturnToStart(Boolean returnToStart) {
		this.returnToStart = returnToStart;
	}

	public int getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}
}
