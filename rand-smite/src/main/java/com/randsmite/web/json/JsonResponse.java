package com.randsmite.web.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;


/**
 * The Class JsonResponse.
 */
public class JsonResponse implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2614486413984204707L;

	/** The success messages. */
    private List<String> successMessages = new ArrayList<String>();

    /** The global errors. */
    private List<String> globalErrors = new ArrayList<String>();

    /** The field errors. */
    private Map<String, String> fieldErrors = new HashMap<String, String>();

    /** The warning messages. */
    private List<String> warningMessages = new ArrayList<String>();

    /**
     * Adds the success message.
     * 
     * @param message the message
     */
    public void addSuccessMessage(String message) {
        successMessages.add(message);
    }

    /**
     * Adds the global error message.
     * 
     * @param message the message
     */
    public void addGlobalErrorMessage(String message) {
        globalErrors.add(message);
    }

    /**
     * Adds the field error message.
     * 
     * @param field the field
     * @param message the message
     */
    public void addFieldErrorMessage(String field, String message) {
        fieldErrors.put(field, message);
    }

    /**
     * Adds the warning message.
     * 
     * @param message the message
     */
    public void addWarningMessage(String message) {
        warningMessages.add(message);
    }

    /**
     * Gets the success messages.
     * 
     * @return the success messages
     */
    public List<String> getSuccessMessages() {
        return Collections.unmodifiableList(successMessages);
    }

    /**
     * Gets the global errors.
     * 
     * @return the global errors
     */
    public List<String> getGlobalErrors() {
        return Collections.unmodifiableList(globalErrors);
    }

    /**
     * Gets the field errors.
     * 
     * @return the field errors
     */
    public Map<String, String> getFieldErrors() {
        return Collections.unmodifiableMap(fieldErrors);
    }

    /**
     * Gets the warning messages.
     * 
     * @return the warning messages
     */
    public List<String> getWarningMessages() {
        return Collections.unmodifiableList(warningMessages);
    }

    /**
     * Adds the errors from binding result.
     * 
     * @param bindingResult the binding result
     * @param locale the locale
     * @param messageSource the message source
     */
    public void addErrorsFromBindingResult(BindingResult bindingResult,
            Locale locale, MessageSource messageSource) {
        Iterator<ObjectError> globalErrors = bindingResult.getGlobalErrors()
                .iterator();
        while (globalErrors.hasNext()) {
            ObjectError error = globalErrors.next();
            addGlobalErrorMessage(messageSource.getMessage(error, locale));
        }
        Iterator<FieldError> fieldErrors = bindingResult.getFieldErrors()
                .iterator();
        while (fieldErrors.hasNext()) {
            FieldError error = fieldErrors.next();
            addFieldErrorMessage(error.getField(),
                    messageSource.getMessage(error, locale));
        }
    }
    
    /**
     * Removes all messages of every type.
     */
    public void clearMessages() {
    	 successMessages = new ArrayList<String>();
    	 globalErrors = new ArrayList<String>();
    	 fieldErrors = new HashMap<String, String>();
    	 warningMessages = new ArrayList<String>();
    }
}
