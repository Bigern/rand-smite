package com.randsmite.web.json;

import java.util.Collection;
import java.util.Collections;

/**
 * The Class DataJsonResponse.
 */
public class DataJsonResponse extends JsonResponse {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7736119261235178768L;

	/** The data. */
    private Collection<?> data = Collections.EMPTY_LIST;
    
    /**
     * Gets the data.
     * 
     * @return the data
     */
    public Collection<?> getData() {
        return data;
    }

    /**
     * Sets the data.
     * 
     * @param data the new data
     */
    public void setData(Collection<?> data) {
        this.data = data;
    }
}
