package com.randsmite.web.entity;

public class Ability {

	private Item itemDescription;

	/**
	 * @return the itemDescription
	 */
	public Item getItemDescription() {
		return itemDescription;
	}

	/**
	 * @param itemDescription
	 *            the itemDescription to set
	 */
	public void setItemDescription(Item itemDescription) {
		this.itemDescription = itemDescription;
	}

}
