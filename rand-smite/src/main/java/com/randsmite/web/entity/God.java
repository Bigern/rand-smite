package com.randsmite.web.entity;


public class God {

	private String Ability1;
	private String Ability2;
	private String Ability3;
	private String Ability4;
	private String Ability5;
	private Double AttackSpeed;
	private Double AttackSpeedPerLevel;
	private String Cons;
	private Double HP5PerLevel;
	private Integer Health;
	private Integer HealthPerFive;
	private Integer HealthPerLevel;
	private String Lore;
	private Double MP5PerLevel;
	private Integer MagicProtection;
	private Double MagicProtectionPerLevel;
	private Integer Mana;
	private Double ManaPerFive;
	private Integer ManaPerLevel;
	private String Name;
	private String OnFreeRotation;
	private String Pantheon;
	private Integer PhysicalPower;
	private Double PhysicalPowerPerLevel;
	private Integer PhysicalProtection;
	private Double PhysicalProtectionPerLevel;
	private String Pros;
	private String Roles;
	private Integer Speed;
	private String Title;
	private String Type;
	private Integer id;
	private Ability abilityDescription1;
	private Ability abilityDescription2;
	private Ability abilityDescription3;
	private Ability abilityDescription4;
	private Ability abilityDescription5;
	private Ability basicAttack;

	/**
	 * @return the ability1
	 */
	public String getAbility1() {
		return Ability1;
	}

	/**
	 * @param ability1
	 *            the ability1 to set
	 */
	public void setAbility1(String ability1) {
		Ability1 = ability1;
	}

	/**
	 * @return the ability2
	 */
	public String getAbility2() {
		return Ability2;
	}

	/**
	 * @param ability2
	 *            the ability2 to set
	 */
	public void setAbility2(String ability2) {
		Ability2 = ability2;
	}

	/**
	 * @return the ability3
	 */
	public String getAbility3() {
		return Ability3;
	}

	/**
	 * @param ability3
	 *            the ability3 to set
	 */
	public void setAbility3(String ability3) {
		Ability3 = ability3;
	}

	/**
	 * @return the ability4
	 */
	public String getAbility4() {
		return Ability4;
	}

	/**
	 * @param ability4
	 *            the ability4 to set
	 */
	public void setAbility4(String ability4) {
		Ability4 = ability4;
	}

	/**
	 * @return the ability5
	 */
	public String getAbility5() {
		return Ability5;
	}

	/**
	 * @param ability5
	 *            the ability5 to set
	 */
	public void setAbility5(String ability5) {
		Ability5 = ability5;
	}

	/**
	 * @return the attackSpeed
	 */
	public Double getAttackSpeed() {
		return AttackSpeed;
	}

	/**
	 * @param attackSpeed
	 *            the attackSpeed to set
	 */
	public void setAttackSpeed(Double attackSpeed) {
		AttackSpeed = attackSpeed;
	}

	/**
	 * @return the attackSpeedPerLevel
	 */
	public Double getAttackSpeedPerLevel() {
		return AttackSpeedPerLevel;
	}

	/**
	 * @param attackSpeedPerLevel
	 *            the attackSpeedPerLevel to set
	 */
	public void setAttackSpeedPerLevel(Double attackSpeedPerLevel) {
		AttackSpeedPerLevel = attackSpeedPerLevel;
	}

	/**
	 * @return the cons
	 */
	public String getCons() {
		return Cons;
	}

	/**
	 * @param cons
	 *            the cons to set
	 */
	public void setCons(String cons) {
		Cons = cons;
	}

	/**
	 * @return the hP5PerLevel
	 */
	public Double getHP5PerLevel() {
		return HP5PerLevel;
	}

	/**
	 * @param hP5PerLevel
	 *            the hP5PerLevel to set
	 */
	public void setHP5PerLevel(Double hP5PerLevel) {
		HP5PerLevel = hP5PerLevel;
	}

	/**
	 * @return the health
	 */
	public Integer getHealth() {
		return Health;
	}

	/**
	 * @param health
	 *            the health to set
	 */
	public void setHealth(Integer health) {
		Health = health;
	}

	/**
	 * @return the healthPerFive
	 */
	public Integer getHealthPerFive() {
		return HealthPerFive;
	}

	/**
	 * @param healthPerFive
	 *            the healthPerFive to set
	 */
	public void setHealthPerFive(Integer healthPerFive) {
		HealthPerFive = healthPerFive;
	}

	/**
	 * @return the healthPerLevel
	 */
	public Integer getHealthPerLevel() {
		return HealthPerLevel;
	}

	/**
	 * @param healthPerLevel
	 *            the healthPerLevel to set
	 */
	public void setHealthPerLevel(Integer healthPerLevel) {
		HealthPerLevel = healthPerLevel;
	}

	/**
	 * @return the lore
	 */
	public String getLore() {
		return Lore;
	}

	/**
	 * @param lore
	 *            the lore to set
	 */
	public void setLore(String lore) {
		Lore = lore;
	}

	/**
	 * @return the mP5PerLevel
	 */
	public Double getMP5PerLevel() {
		return MP5PerLevel;
	}

	/**
	 * @param mP5PerLevel
	 *            the mP5PerLevel to set
	 */
	public void setMP5PerLevel(Double mP5PerLevel) {
		MP5PerLevel = mP5PerLevel;
	}

	/**
	 * @return the magicProtection
	 */
	public Integer getMagicProtection() {
		return MagicProtection;
	}

	/**
	 * @param magicProtection
	 *            the magicProtection to set
	 */
	public void setMagicProtection(Integer magicProtection) {
		MagicProtection = magicProtection;
	}

	/**
	 * @return the magicProtectionPerLevel
	 */
	public Double getMagicProtectionPerLevel() {
		return MagicProtectionPerLevel;
	}

	/**
	 * @param magicProtectionPerLevel
	 *            the magicProtectionPerLevel to set
	 */
	public void setMagicProtectionPerLevel(Double magicProtectionPerLevel) {
		MagicProtectionPerLevel = magicProtectionPerLevel;
	}

	/**
	 * @return the mana
	 */
	public Integer getMana() {
		return Mana;
	}

	/**
	 * @param mana
	 *            the mana to set
	 */
	public void setMana(Integer mana) {
		Mana = mana;
	}

	/**
	 * @return the manaPerFive
	 */
	public Double getManaPerFive() {
		return ManaPerFive;
	}

	/**
	 * @param manaPerFive
	 *            the manaPerFive to set
	 */
	public void setManaPerFive(Double manaPerFive) {
		ManaPerFive = manaPerFive;
	}

	/**
	 * @return the manaPerLevel
	 */
	public Integer getManaPerLevel() {
		return ManaPerLevel;
	}

	/**
	 * @param manaPerLevel
	 *            the manaPerLevel to set
	 */
	public void setManaPerLevel(Integer manaPerLevel) {
		ManaPerLevel = manaPerLevel;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the onFreeRotation
	 */
	public String getOnFreeRotation() {
		return OnFreeRotation;
	}

	/**
	 * @param onFreeRotation
	 *            the onFreeRotation to set
	 */
	public void setOnFreeRotation(String onFreeRotation) {
		OnFreeRotation = onFreeRotation;
	}

	/**
	 * @return the pantheon
	 */
	public String getPantheon() {
		return Pantheon;
	}

	/**
	 * @param pantheon
	 *            the pantheon to set
	 */
	public void setPantheon(String pantheon) {
		Pantheon = pantheon;
	}

	/**
	 * @return the physicalPower
	 */
	public Integer getPhysicalPower() {
		return PhysicalPower;
	}

	/**
	 * @param physicalPower
	 *            the physicalPower to set
	 */
	public void setPhysicalPower(Integer physicalPower) {
		PhysicalPower = physicalPower;
	}

	/**
	 * @return the physicalPowerPerLevel
	 */
	public Double getPhysicalPowerPerLevel() {
		return PhysicalPowerPerLevel;
	}

	/**
	 * @param physicalPowerPerLevel
	 *            the physicalPowerPerLevel to set
	 */
	public void setPhysicalPowerPerLevel(Double physicalPowerPerLevel) {
		PhysicalPowerPerLevel = physicalPowerPerLevel;
	}

	/**
	 * @return the physicalProtection
	 */
	public Integer getPhysicalProtection() {
		return PhysicalProtection;
	}

	/**
	 * @param physicalProtection
	 *            the physicalProtection to set
	 */
	public void setPhysicalProtection(Integer physicalProtection) {
		PhysicalProtection = physicalProtection;
	}

	/**
	 * @return the physicalProtectionPerLevel
	 */
	public Double getPhysicalProtectionPerLevel() {
		return PhysicalProtectionPerLevel;
	}

	/**
	 * @param physicalProtectionPerLevel
	 *            the physicalProtectionPerLevel to set
	 */
	public void setPhysicalProtectionPerLevel(Double physicalProtectionPerLevel) {
		PhysicalProtectionPerLevel = physicalProtectionPerLevel;
	}

	/**
	 * @return the pros
	 */
	public String getPros() {
		return Pros;
	}

	/**
	 * @param pros
	 *            the pros to set
	 */
	public void setPros(String pros) {
		Pros = pros;
	}

	/**
	 * @return the roles
	 */
	public String getRoles() {
		return Roles;
	}

	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(String roles) {
		Roles = roles;
	}

	/**
	 * @return the speed
	 */
	public Integer getSpeed() {
		return Speed;
	}

	/**
	 * @param speed
	 *            the speed to set
	 */
	public void setSpeed(Integer speed) {
		Speed = speed;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return Title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		Title = title;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return Type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		Type = type;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the abilityDescription1
	 */
	public Ability getAbilityDescription1() {
		return abilityDescription1;
	}

	/**
	 * @param abilityDescription1
	 *            the abilityDescription1 to set
	 */
	public void setAbilityDescription1(Ability abilityDescription1) {
		this.abilityDescription1 = abilityDescription1;
	}

	/**
	 * @return the abilityDescription2
	 */
	public Ability getAbilityDescription2() {
		return abilityDescription2;
	}

	/**
	 * @param abilityDescription2
	 *            the abilityDescription2 to set
	 */
	public void setAbilityDescription2(Ability abilityDescription2) {
		this.abilityDescription2 = abilityDescription2;
	}

	/**
	 * @return the abilityDescription3
	 */
	public Ability getAbilityDescription3() {
		return abilityDescription3;
	}

	/**
	 * @param abilityDescription3
	 *            the abilityDescription3 to set
	 */
	public void setAbilityDescription3(Ability abilityDescription3) {
		this.abilityDescription3 = abilityDescription3;
	}

	/**
	 * @return the abilityDescription4
	 */
	public Ability getAbilityDescription4() {
		return abilityDescription4;
	}

	/**
	 * @param abilityDescription4
	 *            the abilityDescription4 to set
	 */
	public void setAbilityDescription4(Ability abilityDescription4) {
		this.abilityDescription4 = abilityDescription4;
	}

	/**
	 * @return the abilityDescription5
	 */
	public Ability getAbilityDescription5() {
		return abilityDescription5;
	}

	/**
	 * @param abilityDescription5
	 *            the abilityDescription5 to set
	 */
	public void setAbilityDescription5(Ability abilityDescription5) {
		this.abilityDescription5 = abilityDescription5;
	}

	/**
	 * @return the basicAttack
	 */
	public Ability getBasicAttack() {
		return basicAttack;
	}

	/**
	 * @param basicAttack
	 *            the basicAttack to set
	 */
	public void setBasicAttack(Ability basicAttack) {
		this.basicAttack = basicAttack;
	}

}