package com.randsmite.web.entity;

import java.util.List;

public class Item {

	private String cooldown;
	private String cost;
	private String description;
	private List<SubItem> menuitems;
	private List<SubItem> rankitems;

	/**
	 * @return the cooldown
	 */
	public String getCooldown() {
		return cooldown;
	}

	/**
	 * @param cooldown
	 *            the cooldown to set
	 */
	public void setCooldown(String cooldown) {
		this.cooldown = cooldown;
	}

	/**
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the menuitems
	 */
	public List<SubItem> getMenuitems() {
		return menuitems;
	}

	/**
	 * @param menuitems
	 *            the menuitems to set
	 */
	public void setMenuitems(List<SubItem> menuitems) {
		this.menuitems = menuitems;
	}

	/**
	 * @return the rankitems
	 */
	public List<SubItem> getRankitems() {
		return rankitems;
	}

	/**
	 * @param rankitems
	 *            the rankitems to set
	 */
	public void setRankitems(List<SubItem> rankitems) {
		this.rankitems = rankitems;
	}

}
