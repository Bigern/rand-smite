package com.randsmite.web.api;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.randsmite.web.utils.APICallerService;

@Controller
@RequestMapping("/lookup")
public class LookupController {

	@Autowired
	private APICallerService apiSetupHelper;

	@RequestMapping(value = "/session", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getSession() {
		final String session = apiSetupHelper.createSession();
		return session;
	}

	@RequestMapping(value = "/player", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public JSONArray getPlayer() {
		final JSONArray session = apiSetupHelper.getPlayer();
		return session;
	}

	@RequestMapping(value = "/match", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public JSONArray getMatch() {
		final JSONArray session = apiSetupHelper.getMatch();
		return session;
	}
}
