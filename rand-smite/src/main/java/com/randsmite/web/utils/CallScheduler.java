package com.randsmite.web.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("scheduler")
public class CallScheduler {

	private String SESSION_ID;

	@Autowired
	private APICallerService apiSetupHelper;

	@Autowired
	private GodManager godManager;

	@PostConstruct
	public void onServerLoad() {
		SESSION_ID = apiSetupHelper.createSession();
		godManager.getGods(SESSION_ID);
	}
}
