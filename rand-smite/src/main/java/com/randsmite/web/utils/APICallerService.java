package com.randsmite.web.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class APICallerService {

	private final String DEV_ID = "1386";

	private final String AUTH_KEY = "53FB152AF2FD41B6883AA4F61EFDFF75";

	private final String BASE_API_CALL = "http://api.smitegame.com/smiteapi.svc/";

	@Autowired
	private JsonReader jsonReader;

	public String createSession() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		final String date = dateFormat.format(new Date());
		final String signature = createSignature("createsession", date);
		final String callUrl = BASE_API_CALL + "createsessionJson/" + DEV_ID
				+ "/" + signature + "/" + date;
		System.out.println("Session ID Call URL: " + callUrl);
		JSONObject jsonData;
		try {
			jsonData = jsonReader.readJsonObjectFromUrl(callUrl);
			final String sessionId = jsonData.getString("session_id");
			System.out.println("SessionId: " + sessionId);
			return sessionId;
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		} catch (final JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONArray getGods(String sessionId) {
		final DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		final String date = dateFormat.format(new Date());
		final String signature = createSignature("getgods", date);
		System.out.println("God API Signature: " + signature);
		final String callUrl = BASE_API_CALL + "getgodsJson/" + DEV_ID + "/"
				+ signature + "/" + sessionId + "/" + date + "/" + 1;
		System.out.println("God Call URL:" + callUrl);
		JSONArray jsonData;
		try {
			jsonData = jsonReader.readJsonFromUrl(callUrl);
			return jsonData;
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		} catch (final JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONArray getPlayer() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		final String date = dateFormat.format(new Date());
		final String signature = createSignature("getplayer", date);
		final String callUrl = BASE_API_CALL + "getplayerJson/" + DEV_ID + "/"
				+ signature + "/" + "B8BA52A902014704965C13D0FC444D30" + "/"
				+ date + "/" + "Overextending";
		JSONArray jsonData;
		try {
			jsonData = jsonReader.readJsonFromUrl(callUrl);
			return jsonData;
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		} catch (final JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONArray getMatch() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		final String date = dateFormat.format(new Date());
		final String signature = createSignature("getmatchdetails", date);
		final String callUrl = BASE_API_CALL + "getmatchdetailsJson/" + DEV_ID
				+ "/" + signature + "/" + "B8BA52A902014704965C13D0FC444D30"
				+ "/" + date + "/" + "139031093";
		JSONArray jsonData;
		try {
			jsonData = jsonReader.readJsonFromUrl(callUrl);
			return jsonData;
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		} catch (final JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String createSignature(String methodCall, String date) {
		final String thingToEncode = DEV_ID + methodCall + AUTH_KEY + date;
		try {
			final MessageDigest messageDigest = MessageDigest
					.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(thingToEncode.getBytes());
			final byte[] digest = messageDigest.digest();
			final BigInteger bigInt = new BigInteger(1, digest);
			String mD5Hash = bigInt.toString(16);
			while (mD5Hash.length() < 32) {
				mD5Hash = "0" + mD5Hash;
			}
			return mD5Hash;
		} catch (final NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
}
