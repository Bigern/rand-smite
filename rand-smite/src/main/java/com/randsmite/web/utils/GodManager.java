package com.randsmite.web.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.randsmite.web.entity.God;

@Service("godManager")
public class GodManager {

	@Autowired
	private APICallerService apiSetupHelper;

	private List<God> godList;

	public JSONArray getGods(String sessionId) {
		godList = new ArrayList<God>();
		final JSONArray godData = apiSetupHelper.getGods(sessionId);
		for (int i = 0; i < godData.length(); i++) {
			try {
				final God god = new Gson().fromJson(godData.getJSONObject(i)
						.toString(), God.class);
				godList.add(god);
			} catch (final JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("God List Successfully Populated");
		return godData;
	}

	/**
	 * @return the godList
	 */
	public List<God> getGodList() {
		return godList;
	}

	/**
	 * @param godList
	 *            the godList to set
	 */
	public void setGodList(List<God> godList) {
		this.godList = godList;
	}
}
